/*
	SDFS client
*/

#include "share.h"
#include <dirent.h>
#include <algorithm>

string argument_check(int n_argument, char *arguments[]);
void get_file(int & socket_file_descriptor, char * sdfs_filename, const char * local_filename);
void get(char * sdfs_filename, char * local_filename);
void remove(char * sdfs_filename);
void get_delete_target(vector<string> & delete_target, map< string, vector<string> > & file_list, char * sdfs_filename);
void listsdfsdirfilename(void);
void listfilelocation(char * filename);

int main(int n_argument, char *arguments[])
{
	string operation = argument_check(n_argument, arguments); // validate and parse arguments to get operation to be performed

	if(operation == "put")
	{
		put(arguments[2], arguments[3], 1); // put operation
	}
	else if(operation == "get")
	{
		get(arguments[2], arguments[3]); // get operation
	}
	else if(operation == "delete")
	{
		remove(arguments[2]); // delete operation
	}
	else if(operation == "store")
	{
	        /* list all files stored in sdfs_file directory */
	        listsdfsdirfilename();
	}
       	else if(operation == "list")
	{
	        listfilelocation(arguments[2]);
	}

	return 0;
}

/**********************************************************************/
/*********************** helper functions *******************************/
/**********************************************************************/

string argument_check(int n_argument, char *arguments[])
{
	if(n_argument != 4 && n_argument != 3 && n_argument != 2)
	{
		fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
		exit(1);
	}
	else
	{
		string operation(arguments[1]);
		if(n_argument == 4) // perform put or get operations
		{
			if(operation != "put" && operation != "get")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else
			{
				return operation;
			}
		}
		else if(n_argument == 3) // perform delete or list operations
		{
			if(operation != "delete" && operation != "list")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else if(operation == "list")
			{
				return operation;
			}
			else if(operation == "delete")
			{
			        return operation;
			}
			else
			{
			        return operation;
			}
		}
		else // perform store operation 
		{
			if(operation != "store")
			{
				fprintf(stderr,"usage:\n\t./sdfs put local_filename sdfs_filename\n\t./sdfs get sdfs_filename local_filename\n\t./sdfs delete sdfs_filename\n\t./sdfs list sdfs_filename\n\t./sdfs store\n");
				exit(1);
			}
			else
			{
				return operation;
			}
		}
	}
}

void get(char * sdfs_filename, char * local_filename)
{

	/* get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	/* modification: handle "file does not exist" case */
	bool file_exist = 0;
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		if(find(iter->second.begin(), iter->second.end(), string(sdfs_filename)) != iter->second.end())
		{
			file_exist = 1;
			break;
		}
	}
	if(!file_exist)
	{
		cout << '"' << string(sdfs_filename) << '"' << " does not exist in SDFS" << endl;
		return;
	}

	string ip_alive;
	vector<string> fl;
	int count = 0;
	for(map<string, vector<string> >::iterator it=file_list.begin(); it!=file_list.end(); it++)
	{  
	  ip_alive = it->first;
	  fl = it->second;
	  if(find(fl.begin(), fl.end(), sdfs_filename) != fl.end())
	  {
	    //cout<<endl<<ip_alive<<endl;
	    if(count)
	      break;
	    count++;
	  }
	  else
	    cout<<"file doesn't exist in local file list!"<<endl;
	}

	/*** connect with 1 IP and get the file ***/
	int socket_file_descriptor;
	make_socket(socket_file_descriptor, ip_alive, GET_PORT);

	/* get file */
	string fn = "local_file/";
	string str(local_filename);
	fn += str;
	//cout<<"local_filename: "<<fn<<endl;
	get_file(socket_file_descriptor, sdfs_filename, fn.c_str());

	if(close(socket_file_descriptor) == -1)
	{
		perror("close");
		exit(1);
	}
}

void get_file(int & socket_file_descriptor, char * sdfs_filename, const char * local_filename)
{
	/*** send sdfs_filename so that server knows the name of file in file system ***/
	string filename = sdfs_filename;
	if(send(socket_file_descriptor, filename.c_str(), (int)(filename.size()), 0) == -1)
	{
		perror("send");
		exit(1);
	}

	/*** read data from server and put it into "local_filename" ***/
	FILE * file_get;
	file_get = fopen(local_filename, "wb");
	if(file_get == NULL)
	{
		fputs("File error", stderr);
		exit(1);
	}

	char buffer[BUFFER_LENGTH];
	memset(&buffer, 0, sizeof buffer); 

	int flag = 0; // if flag is 0, end of head(SDFS filename) has not been encountered
	int received;
	/*ofstream output;
	
	output.open(local_filename);
	if(!output.is_open())
	  {
	    perror("ofstream open");
	    exit(2);
	  }
	*/
	while( (received = recv(socket_file_descriptor, buffer, BUFFER_LENGTH-1, 0)) > 0 )
	{
	        buffer[received] = '\0';
                string content = buffer; // convert C string to C++ string for easier string manipulation  
		//cout<<"content:"<<content<<endl;

		fwrite(buffer, sizeof(char), received, file_get);                    
	        //output << content; 
	}

	// close file stream
	//output.close();
	//output.clear();
}

void remove(char * sdfs_filename)
{
	/* get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	/* get a list of VM's that host "sdfs_filename" */
	vector<string> delete_target;
	get_delete_target(delete_target, file_list, sdfs_filename);
	if(delete_target.empty()) // file to be deleted does not exist in SDFS
	{
		cout << '"' << string(sdfs_filename) << '"' << " does not exist in SDFS" << endl;
		exit(0);
	}

	/* send DELETE request to corresponding VM's */
	for(size_t i = 0; i < delete_target.size(); i = i+1)
	{
		//cout << delete_target[i] << endl;
		int socket_file_descriptor;
		make_socket(socket_file_descriptor, delete_target[i], DELETE_PORT);
		if(send(socket_file_descriptor, sdfs_filename, sizeof(char)*strlen(sdfs_filename), 0) == -1)
		{
			perror("send");
			exit(1);
		}
	}

	/* update and broadcast file list */
	update_file_list(file_list);
	broadcast_file_list(file_list);

	/*
	char buffer[BUFFER_LENGTH];
	memset(&buffer, 0, sizeof buffer); 
	ssize_t num_recv;
	string response = "";
	while(1)
	{
		num_recv = recv(socket_file_descriptor, buffer, BUFFER_LENGTH-1, 0);

		if(num_recv == -1)
		{
			perror("recv");
			exit(1);
		}

		if(num_recv > 0)
		{
			buffer[num_recv] = '\0';
			response = response + buffer;
		}
		else
		{
			break;
		}
	}

	if(close(socket_file_descriptor) == -1)
	{
		perror("close");
		exit(1);
	}

	if(response.compare("HTTP/1.0 404 Not Found") == 0)
	{
		cout << '"' << string(sdfs_filename) << '"' << " does not exists in SDFS" << endl;
	}
	*/
}

void get_delete_target(vector<string> & delete_target, map< string, vector<string> > & file_list, char * sdfs_filename)
{
	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		for(vector<string>::iterator viter = iter->second.begin(); viter != iter->second.end(); viter++)
		{
			if(viter->compare(string(sdfs_filename)) == 0)
			{
				delete_target.push_back(iter->first);
				break;
			}
		}
	}

	// remove "sdfs_filename" from delete targets(update file list later)
	for(size_t i = 0; i < delete_target.size(); i = i+1)
	{
		file_list[ delete_target[i] ].erase( find(file_list[ delete_target[i] ].begin(), file_list[ delete_target[i] ].end(), string(sdfs_filename)) );
	}
}


void listsdfsdirfilename(void)
{
        
         DIR *dpdf;
	 struct dirent *epdf;
	 dpdf = opendir("./sdfs_file/");
	 if (dpdf != NULL)
	 {
	   while (epdf = readdir(dpdf))
	   {
	     printf("Filename: %s\n",epdf->d_name);
	   }
	 }

}

void listfilelocation(char * filename)
{
	/* get local file list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);

	int hit = 0;
	string ip_alive;
	vector<string> fl;
	for(map<string, vector<string> >::iterator it=file_list.begin(); it!=file_list.end(); it++)
	{  
	  ip_alive = it->first;
	  fl = it->second;
	  if(find(fl.begin(), fl.end(), filename) != fl.end())
	  {
	    cout<<endl<<ip_alive<<endl;
	    hit++;
	  }
	}

	if(!hit)//didn't find file in sdfs
	  cout<<"No such file found in sdfs\n"<<endl;
}

