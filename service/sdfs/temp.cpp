void get(char * sdfs_filename, char * local_filename);
void get_alive_IP(vector<string> & IP_alive);

void get(char * sdfs_filename, char * local_filename)
{
	/*** get a list of IP's that are still alive ***/
	vector<string> IP_alive;
	get_alive_IP(IP_alive);

	/*** connect with 1 IP and get the file ***/
	int socket_file_descriptor;
	make_socket(socket_file_descriptor, IP_alive[0], GET_PORT);
	get_file(socket_file_descriptor, sdfs_filename, local_filename);
	if(close(socket_file_descriptor) == -1)
	{
		perror("close");
		exit(1);
	}
}

void get_file(int & socket_file_descriptor, char * sdfs_filename, char * local_filename)
{
	/*** send sdfs_filename so that server knows the name of file in file system ***/
	string filename = sdfs_filename;
	if(send(socket_file_descriptor, filename.c_str(), (int)(filename.size()), 0) == -1)
	{
		perror("send");
		exit(1);
	}

	/*** read data from server and put it into "local_filename" ***/
	FILE * file_get;
	file_get = fopen(local_filename, "wb");
	if(file_get == NULL)
	{
		fputs("File error", stderr);
		exit(1);
	}

	char buffer[BUFFER_LENGTH];
	memset(&buffer, 0, sizeof buffer); 

	int flag = 0; // if flag is 0, end of head(SDFS filename) has not been encountered
	int recieved;
	ofstream output;
	while( (recieved = recv(socket_file_descriptor, buffer, BUFFER_LENGTH-1, 0)) > 0 )
	{
		buffer[recieved] = '\0';
		string content = buffer; // convert C string to C++ string for easier string manipulation
		if(!flag)
		{
			if( string(content.substr(0, content.find("\r\n\r\n"))).compare("HTTP/1.0 404 Not Found") == 0 )
			{
				cout << '"' << string(sdfs_filename) << '"' << "does not exists in SDFS" << endl;
				exit(0);
			}
			else
			{
				/*** open file to write ***/
				output.open(local_filename);
				if(output.is_open())
				{
					output << content.substr(content.find("\r\n\r\n")+4);
					flag = 1;
				}
				else
				{
					perror("ofstream open");
					exit(2);
				}	
			}
		}
		else
		{
			output << content;
		}
	}

	// close file stream
	output.close();
	output.clear();
}
