#ifndef SHARE_H
#define SHARE_H

/*
	Shared functions called by SDFS client and server
*/

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#define PUT_PORT "3492" // the port client will be connecting to for put operation
#define GET_PORT "3493" // get operation
#define DELETE_PORT "3494" // delete operation
#define BROADCAST_PORT "3495" // file list broadcast
#define JOIN_PORT "3496" // node joins cluster
#define REPLICATE_PORT "3497" // replicate lost files(handle node failure)

#define BUFFER_LENGTH 60000

#define NUM_REPLICA 3 // number of replicas of each file in cluster

/* send local_filename to SDFS so that the enture cluster has 3 replicas of this file */
void put(const char * local_filename, const char * sdfs_filename, bool from_local);
/* helper functions */
void get_file_list(map< string, vector<string> > & file_list);
void read_file_list(string & raw);
void get_put_target(vector<string> & put_target, map< string, vector<string> > & file_list, const char * sdfs_filename);
void inorder_insert(vector<string> & target_candidate, map< string, vector<string> > & file_list, string key, size_t value);
void make_socket(int & socket_file_descriptor, string & IP, const char * port);
void send_file(int & socket_file_descriptor, const char * local_filename, const char * sdfs_filename, bool from_local);
void update_file_list(map< string, vector<string> > & file_list);
void broadcast_file_list(map< string, vector<string> > & file_list);
void send_file_list(int & socket_file_descriptor);

/* node join */
string get_selfIP();

/* for debugging purpose */
void print_file_list(map< string, vector<string> > & file_list, string identifier);

#endif
